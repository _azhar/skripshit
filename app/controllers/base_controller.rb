class BaseController < ApplicationController

  def show
    @user = User.find_by_private_broadcast(params[:id])
    redirect_to root_path if @user.blank?
    render layout: 'show_layout' if !@user.blank?
  end

  def index
    if user_signed_in?
      redirect_to broadcast_path(current_user.hash_key)
    else
      render layout: 'home_layout'
    end
  end
end
