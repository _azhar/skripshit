class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  before_save :generate_token

  def generate_token
    self.private_broadcast = SecureRandom.hex(3)
  end

  def hash_key
    self.private_broadcast
  end

  def to_param
    self.private_broadcast
  end

end
